from django.shortcuts import render, redirect
from .forms import CreateEvent, CreateMember
from .models import Event, Member

# Create your views here.
def indexEvent(request):
    event = Event.objects.all()
    member = Member.objects.all()
    return render(request, 'eventList.html', {'event' : event, "member" : member})

def addEvent(request):
    if request.method == 'POST':
        form = CreateEvent(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story6:indexEvent')
    return render(request, 'addEvent.html')

def addMember(request, id = 0):
    id = int(id)
    event = Event.objects.get(id=id)
    if request.method == 'POST':
        id = request.POST['eventId']
        name = request.POST['name']
        event = Event.objects.get(id=id)
        data = {'name' : name, 'event' : event}
        form = CreateMember(data=data)
        if form.is_valid():
            form.save()
            return redirect('story6:indexEvent')
    return render(request, 'addMember.html', {'event' : event})

def deleteMember(request, id):
    id = int(id)
    member = Member.objects.get(id=id)
    member.delete()
    return redirect('story6:indexEvent')