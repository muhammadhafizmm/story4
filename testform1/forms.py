from django import forms
from . import models

class CreateMatkul(forms.ModelForm):
    class Meta:
        model = models.MataKuliah
        fields = ['namaMatkul', 'dosen', 'jumlahSKS', 'deskripsiMatkul', 'waktu', 'semester']