from django.urls import path
from . import views

app_name = "matakuliah"


urlpatterns = [
    path('', views.index, name='index'),
    path('show/<str:order>/', views.show, name='show'),
    path('showAdmin/<str:order>/', views.showAdmin, name='showAdmin'),
    path('create', views.create, name='create'),
    path('indexAdmin', views.indexAdmin, name='indexAdmin'),
    path('indexAdmin/<str:order>/', views.indexAdmin, name='indexAdmin'),
]