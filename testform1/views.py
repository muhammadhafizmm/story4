from django.shortcuts import render, redirect
from .models import MataKuliah, Tugas
from . import forms
import datetime

# Create your views here.
def index(request):
    matakuliah = MataKuliah.objects.all()
    return render(request, 'matkul.html', {'matakuliah' : matakuliah})

def show(request, order):
    order = int(order)
    matakuliah = MataKuliah.objects.get(order=order)
    tugas = Tugas.objects.filter(matakuliah=matakuliah)
    today = datetime.datetime.now().timestamp()
    masihLama = []
    besok = []
    udahLewat = []
    for item in tugas:
        if item.tanggal.timestamp() < today:
            udahLewat.append(item)
        elif item.tanggal.timestamp() < today + 86400:
            besok.append(item)
        else:
            masihLama.append(item)

    return render(request, "show.html", {"matakuliah" : matakuliah, "besok" : besok, "udahLewat" : udahLewat, "masihLama" : masihLama})

def showAdmin(request, order):
    order = int(order)
    matakuliah = MataKuliah.objects.get(order=order)
    return render(request, "showAdmin.html", {"matakuliah" : matakuliah})

def create(request):
    if (request.method == 'GET'):
        form = forms.CreateMatkul()
        return render(request, "create.html", {'form' : form})
    elif (request.method == 'POST'):
        form = forms.CreateMatkul(request.POST)
        if form.is_valid():
            form.save()
            return redirect('matakuliah:index')

def indexAdmin(request):
    if request.method == "POST":
        order = request.POST["order"]
        matakuliah = MataKuliah.objects.get(order=order)
        matakuliah.delete()
        return redirect('matakuliah:indexAdmin')
    else:
        matakuliah = MataKuliah.objects.all()
        return render(request, 'indexAdmin.html', {'matakuliah' : matakuliah})
