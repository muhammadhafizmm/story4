from django.urls import path
from . import views

app_name = "polls"

urlpatterns = [
    path('', views.index, name='index'),
    path('redSoon/', views.redSoon, name='redSoon'),
    path('yellowSoon/', views.yellowSoon, name='yellowSoon'),
    path('time/', views.time, name='time'),
    path('time/<str:time>', views.time, name='time'),
]