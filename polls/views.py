import datetime
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, "index.html", {'static' : 'true', 'color' : 'none'})
def redSoon(request):
    color = 'red'
    return render(request, "redSoon.html", {'color' : color})
def yellowSoon(request):
    color = 'yellow'
    return render(request, "yellowSoon.html", {'color' : color})
def time(request, time = '0'):
    time = int(time)
    time = time + 7
    TIME_ZONE = datetime.datetime.now() + datetime.timedelta(hours = time)
    return render(request, "time.html", {'time' : TIME_ZONE})
